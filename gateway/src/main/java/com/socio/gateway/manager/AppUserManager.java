package com.socio.gateway.manager;

import com.socio.gateway.config.UserClient;
import com.socio.gateway.model.AppUser;
import com.socio.gateway.model.JWTRequest;
import com.socio.gateway.model.JWTResponse;
import com.socio.gateway.model.Response;
import com.socio.gateway.utility.ErrorCodes;
import com.socio.gateway.utility.JWTUtility;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import retrofit2.Call;

import java.io.IOException;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.ArrayList;

@Service
public class AppUserManager {

    @Autowired
    private UserClient userClient;

    @Autowired
    private JWTUtility jwtUtility;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public boolean authenticateAppUser(String email,String password) throws IOException {
        AppUser appUser = getAppUserFromEmail(email);
        return passwordEncoder.matches(password,appUser.getPassword());
    }

    public Response<JWTResponse> userLogin(JWTRequest jwtRequest) {
        final String token = jwtUtility.generateToken(jwtRequest.getEmail());
        return new Response<>(new JWTResponse(token));
    }

    public AppUser getAppUserFromEmail(String email) throws IOException {
        Call<Response<AppUser>> appUserCall = userClient.getAppUserByEmail(email);
        retrofit2.Response<Response<AppUser>> response = appUserCall.execute();
        Response<AppUser> responseBody = response.body();
        System.out.println("resp :" + response);
        if (response.isSuccessful()) {
            if (!ObjectUtils.isEmpty(responseBody) && !ObjectUtils.isEmpty(responseBody.getPayload())) {
                return responseBody.getPayload();
            }
            throw new UsernameNotFoundException("user not found");
        }
        throw new UsernameNotFoundException("bad request");
    }
}
