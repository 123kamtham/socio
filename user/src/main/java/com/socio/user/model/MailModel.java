package com.socio.user.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MailModel {

    private String mailSubject;
    private String mailFrom;
    private String mailTo;
    private String mailContent;
}
