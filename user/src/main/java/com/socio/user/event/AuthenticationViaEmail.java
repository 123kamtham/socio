package com.socio.user.event;

import com.socio.user.entity.AppUser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class AuthenticationViaEmail extends ApplicationEvent {

    private final AppUser appUser;
    private final String applicationUrl;
    private final String redirectRequestPath;

    public AuthenticationViaEmail(AppUser appUser, String applicationUrl, String redirectRequestPath) {
        super(appUser);
        this.appUser = appUser;
        this.applicationUrl = applicationUrl;
        this.redirectRequestPath = redirectRequestPath;
    }
}
