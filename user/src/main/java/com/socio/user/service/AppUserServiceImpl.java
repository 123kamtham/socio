package com.socio.user.service;

import com.socio.user.entity.AppUser;
import com.socio.user.entity.VerificationToken;
import com.socio.user.enums.AppUserRole;
import com.socio.user.event.AuthenticationViaEmail;
import com.socio.user.model.*;
import com.socio.user.repository.AppUserRepository;
import com.socio.user.repository.VerificationTokenRepository;
import com.socio.user.unitily.ErrorCodes;
//import com.socio.user.unitily.JWTUtility;
import com.socio.user.unitily.RedirectRequestPath;
import com.socio.user.unitily.StringConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Slf4j
@Service
public class AppUserServiceImpl implements AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private MailService mailService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

//    @Autowired
//    private JWTUtility jwtUtility;

    @Override
    public Response<Boolean> registerUser(AppUserRegistrationInfo appUserRegistrationInfo, HttpServletRequest request) {
        boolean isUsernameAvailable = isUsernameAvailable(appUserRegistrationInfo.getUsername());
        if (!isUsernameAvailable) {
            return new Response<>(true, false, Collections.singletonList(new ErrorModel(ErrorCodes.BAD_REQUEST, StringConstants.USERNAME_NOT_AVAILABLE)));
        }
        AppUser appUser = AppUser.builder()
                .firstname(appUserRegistrationInfo.getFirstName())
                .lastname(appUserRegistrationInfo.getLastName())
                .username(appUserRegistrationInfo.getUsername())
                .email(appUserRegistrationInfo.getEmail())
                .password(passwordEncoder.encode(appUserRegistrationInfo.getPassword()))
                .role(AppUserRole.COMMON_USER)
                .build();
        appUserRepository.save(appUser);
        applicationEventPublisher.publishEvent(new AuthenticationViaEmail(appUser, getApplicationUrl(request), RedirectRequestPath.VERIFY_REGISTRATION));
        return new Response<>(true,true,null);

    }

    private String getApplicationUrl(HttpServletRequest request) {
        return "http://" +
                request.getServerName() +
                ":" +
                request.getServerPort() +
                request.getContextPath();
    }

    //username in User is email in AppUser
//    @Override
//    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//        AppUser appUser = appUserRepository.findByEmail(email);
//        return new User(appUser.getEmail(), appUser.getPassword(),appUser.isEnabled(),true,true,true, new ArrayList<>());
//    }

    @Override
    public void saveTokenForUser(String token, AppUser appUser) {
        VerificationToken verificationToken = new VerificationToken(token, appUser);
        verificationTokenRepository.save(verificationToken);
    }

    @Override
    public void saveAuthenticationToken(AppUser appUser, String applicationUrl, String redirectToRequestPath) {
        String token = UUID.randomUUID().toString();
        saveTokenForUser(token, appUser);
        String verificationUrl = applicationUrl
                + redirectToRequestPath + "?token="
                + token;

        mailService.sendRedirectRequestPathViaMail(redirectToRequestPath, appUser.getEmail(), verificationUrl);
    }
    @Override
    public Response<Boolean> requestResetPassword(String email, HttpServletRequest request) {
        AppUser appUser = appUserRepository.findByEmail(email);
        if (Objects.isNull(appUser)) {
            return new Response<>(true, false, Collections.singletonList(new ErrorModel(ErrorCodes.NOT_FOUND, StringConstants.USER_NOT_EXIST)));
        }
        applicationEventPublisher.publishEvent(new AuthenticationViaEmail(appUser, getApplicationUrl(request), RedirectRequestPath.RESET_PASSWORD));
        return new Response<>(true,true,null);
    }

    @Override
    public Response<Boolean> verifyRegistration(String token) {
        Response<AppUser> response = validateVerificationToken(token);
        if (response.isSuccess()) {
            AppUser appUser = response.getPayload();
            appUser.setEnabled(true);
            appUserRepository.save(appUser);
            return new Response<>(true,true,null);
        }
        return new Response<>(true, false, response.getErrors());
    }

    @Override
    public Response<Boolean> resetPassword(String token, String password) {
        Response<AppUser> response = validateVerificationToken(token);
        if (response.isSuccess()) {
            AppUser appUser = response.getPayload();
            appUser.setPassword(passwordEncoder.encode(password));
            appUserRepository.save(appUser);
            return new Response<>(true,true,null);
        }
        return new Response<>(true, false, response.getErrors());
    }

    @Override
    public Response<Boolean> changePassword(PasswordModel passwordModel) {
        AppUser appUser = appUserRepository.findByEmail(passwordModel.getEmail());
        if (!checkifValidOldPassword(appUser, passwordModel.getOldPassword())) {
            return new Response<>(true, false, Collections.singletonList(new ErrorModel(ErrorCodes.UNAUTHORIZED, "Invalid Old Password")));
        }
        appUser.setPassword(passwordEncoder.encode(passwordModel.getNewPassword()));
        appUserRepository.save(appUser);
        return new Response<>(true,true,null);
    }

    @Override
    public boolean isUsernameAvailable(String username) {
        boolean isUsernameExist = appUserRepository.existsByUsername(username);
        return !isUsernameExist;
    }

    @Override
    public Response<Boolean> updateAppUserProfile(AppUserProfile appUserProfile) {
        String username = appUserProfile.getUsername();
        String email = appUserProfile.getEmail();
        AppUser appUser = appUserRepository.findByEmail(email);
        if (Objects.isNull(appUser)) {
            return new Response<>(true, false, Collections.singletonList(new ErrorModel(ErrorCodes.NOT_FOUND, StringConstants.USER_NOT_EXIST)));
        }
        if (!appUser.getUsername().equals(username) && !isUsernameAvailable(username)) {
            return new Response<>(true, false, Collections.singletonList(new ErrorModel(ErrorCodes.BAD_REQUEST, StringConstants.USERNAME_NOT_AVAILABLE)));
        }
        appUser.setUsername(appUserProfile.getUsername());
        appUser.setFirstname(appUserProfile.getFirstname());
        appUser.setLastname(appUserProfile.getLastname());
        appUser.setBio(appUserProfile.getBio());
        appUserRepository.save(appUser);
        return new Response<>(true,true,null);
    }

    @Override
    public Response<AppUserProfile> getAppUserProfile(String email) {
        AppUser appUser = appUserRepository.findByEmail(email);
        if (Objects.isNull(appUser)) {
            return new Response<>(true, null, Collections.singletonList(new ErrorModel(ErrorCodes.NOT_FOUND, StringConstants.USER_NOT_EXIST)));
        }
        AppUserProfile appUserProfile = AppUserProfile.builder()
                .username(appUser.getUsername())
                .firstname(appUser.getFirstname())
                .lastname(appUser.getLastname())
                .bio(appUser.getBio())
                .email(email)
                .build();
        return new Response<>(true, appUserProfile, null);
    }

    @Override
    public Response<AppUser> getAppUserByEmail(String email) {
        AppUser appUser = appUserRepository.findByEmail(email);
        if (Objects.isNull(appUser)) {
            return new Response<>(true, null, Collections.singletonList(new ErrorModel(ErrorCodes.NOT_FOUND, StringConstants.USER_NOT_EXIST)));
        }
        return new Response<>(true,appUser,null);
    }

    private boolean checkifValidOldPassword(AppUser appUser, String oldPassword) {
        return passwordEncoder.matches(oldPassword,appUser.getPassword());
    }

    private Response<AppUser> validateVerificationToken(String token) {
        VerificationToken verificationToken = verificationTokenRepository.findByToken(token);
        if (Objects.isNull(verificationToken)) {
            return new Response<>(true, null, Collections.singletonList(new ErrorModel(ErrorCodes.NOT_FOUND, "invalid token")));
        }
        AppUser appUser = verificationToken.getAppUser();
        if (isTokenExpired(verificationToken)) {
            return new Response<>(true, null, Collections.singletonList(new ErrorModel(ErrorCodes.REQUEST_EXPIRED, "token expired")));
        }
        return new Response<>(appUser);
    }

    private boolean isTokenExpired(VerificationToken verificationToken) {
        long expirationDate = verificationToken.getExpirationDate().getTime();
        long currenttime = Calendar.getInstance().getTime().getTime();
        return (expirationDate < currenttime);
    }


}
