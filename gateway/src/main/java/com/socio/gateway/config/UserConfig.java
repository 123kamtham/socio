package com.socio.gateway.config;

import lombok.extern.log4j.Log4j2;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.concurrent.TimeUnit;

@Log4j2
@Configuration
public class UserConfig {

    private okhttp3.OkHttpClient getHttpClient() {

        int CONNECT_TIMEOUT_MS =  5000;
        int READ_TIMEOUT_MS = 5000;
        int WRITE_TIMEOUT_MS = 5000;
        log.info(CONNECT_TIMEOUT_MS+" "+READ_TIMEOUT_MS+" VALUES1 "+WRITE_TIMEOUT_MS );

        return new okhttp3.OkHttpClient.Builder()
                .addNetworkInterceptor(chain -> {
                    okhttp3.Request request = chain
                            .request()
                            .newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .build();
                    return chain.proceed(request);
                })
                .connectTimeout(CONNECT_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .writeTimeout(WRITE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .build();
    }


    @Bean
    public UserClient userClient(){
        String userBaseUrl = "http://localhost:8081/";
        log.info(userBaseUrl +" AUTHURL");

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(userBaseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(getHttpClient())
                .build();
        return retrofit.create(UserClient.class);
    }
}
