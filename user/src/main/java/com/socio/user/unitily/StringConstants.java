package com.socio.user.unitily;

public class StringConstants {


    public static final String MAIL_FROM = "123kamtham@gmail.com";
    public static final String REGISTRATION_VERIFICATION_MAIL_CONTENT = "Please , Verify Your Account by clicking on the given link /n";
    public static final String REGISTRATION_VERIFICATION_MAIL_SUBJECT = "Account Verification | Socio";
    public static final String RESET_PASSWORD_MAIL_CONTENT = "Please , Set your new Password by clicking on the given link /n";
    public static final String RESET_PASSWORD_MAIL_SUBJECT = "Reset Password | Socio";

    public static final String APP_NAME= "Socio" ;
    public static final String USER_NOT_EXIST = "User does not exist";
    public static final String USERNAME_NOT_AVAILABLE = "username not available";

    public static final String BAD_CRED = "bad credentials";
}
