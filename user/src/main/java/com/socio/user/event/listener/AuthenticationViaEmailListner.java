package com.socio.user.event.listener;

import com.socio.user.event.AuthenticationViaEmail;
import com.socio.user.service.AppUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AuthenticationViaEmailListner implements ApplicationListener<AuthenticationViaEmail> {

    @Autowired
    private AppUserService appUserService;

    @Override
    public void onApplicationEvent(AuthenticationViaEmail event) {
        appUserService.saveAuthenticationToken(event.getAppUser(), event.getApplicationUrl(), event.getRedirectRequestPath());
    }
}
