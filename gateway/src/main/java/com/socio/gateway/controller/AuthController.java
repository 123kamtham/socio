package com.socio.gateway.controller;

import com.socio.gateway.manager.AppUserManager;
import com.socio.gateway.model.*;
import com.socio.gateway.utility.ErrorCodes;
import com.socio.gateway.utility.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
public class AuthController {

    @Autowired
    private AppUserManager appUserManager;

    @PostMapping("/login")
    public Response<JWTResponse> authenticate(@RequestBody JWTRequest jwtRequest) {
        try {
            if(appUserManager.authenticateAppUser(jwtRequest.getEmail(),jwtRequest.getPassword())) {
                return appUserManager.userLogin(jwtRequest);
            }
            return new Response<>(false, null, Collections.singletonList(new ErrorModel(ErrorCodes.UNAUTHORIZED, StringConstants.BAD_CRED)));
        } catch (BadCredentialsException e) {
            return new Response<>(false, null, Collections.singletonList(new ErrorModel(ErrorCodes.UNAUTHORIZED, StringConstants.BAD_CRED)));
        } catch (Exception e) {
            return new Response<>(e, ErrorCodes.INTERNAL_SERVER_ERROR);
        }
    }
}
