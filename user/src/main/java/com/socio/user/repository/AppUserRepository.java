package com.socio.user.repository;


import com.socio.user.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppUserRepository extends JpaRepository<AppUser,Long> {

    AppUser findByEmail(String email);

    boolean existsByUsername(String username);
}
