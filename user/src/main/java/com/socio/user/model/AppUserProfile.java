package com.socio.user.model;

import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class AppUserProfile {
    private String firstname;
    private String lastname;
    private String username;
    private String email;
    private String bio;
}
