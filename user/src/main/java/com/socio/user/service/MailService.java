package com.socio.user.service;


public interface MailService {
    void sendRedirectRequestPathViaMail(String redirectToRequestPath, String email, String verificationUrl);
}
