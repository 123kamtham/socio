package com.socio.user.unitily;

public class RedirectRequestPath {

    public static final String VERIFY_REGISTRATION = "/user/verifyRegistration";
    public static final String RESET_PASSWORD = "/user/resetPassword";
}
