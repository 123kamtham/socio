package com.socio.gateway.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class ErrorModel {
    private int code;
    private String message;
}
