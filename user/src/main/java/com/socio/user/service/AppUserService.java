package com.socio.user.service;



import com.socio.user.entity.AppUser;
import com.socio.user.model.*;
//import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.http.HttpServletRequest;

public interface AppUserService {
    Response<Boolean> registerUser(AppUserRegistrationInfo appUserRegistrationInfo, HttpServletRequest request);

    void saveTokenForUser(String token, AppUser appUser);

    void saveAuthenticationToken(AppUser appUser, String applicationUrl, String redirectToRequestPath);

    Response<Boolean> requestResetPassword(String email, HttpServletRequest request);

    Response<Boolean> verifyRegistration(String token);

    Response<Boolean> resetPassword(String token, String password);

    Response<Boolean> changePassword(PasswordModel passwordModel);

    boolean isUsernameAvailable(String username);

    Response<Boolean> updateAppUserProfile(AppUserProfile appUserProfile);

    Response<AppUserProfile> getAppUserProfile(String email);

    Response<AppUser> getAppUserByEmail(String email);
}
