package com.socio.gateway.config;

import com.socio.gateway.model.AppUser;
import com.socio.gateway.model.Response;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserClient {

    @GET("/user/getAppUserByEmail")
    Call<Response<AppUser>> getAppUserByEmail(
            @Query("email") String email
    );
}
