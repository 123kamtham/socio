package com.socio.gateway.utility;

public class ErrorCodes {
    public static final int INTERNAL_SERVER_ERROR = 500;
    public static final int NO_CONTENT = 204;
    public static final int REQUEST_EXPIRED = 209;
    public static final int UNAUTHORIZED = 401;
    public static final int NOT_FOUND = 404;

    public static final int BAD_REQUEST = 400;
}
