package com.socio.user.entity;

import com.socio.user.enums.AppUserRole;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(
        name = "appUser",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "email_address_unique",
                        columnNames = "email_address"),
                @UniqueConstraint(
                        name = "username_unique",
                        columnNames = "username"
                )
        }
)
public class AppUser implements Serializable {

    @Id
    @SequenceGenerator(
            name = "app_user_sequence",
            sequenceName = "app_user_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "app_user_sequence"
    )
    private Long id;
    private String firstname;
    private String lastname;
    @Column(
            name = "username",
            nullable = false
    )
    private String username;
    @Column(
            name = "email_address",
            nullable = false
    )
    private String email;
    @Column(length = 60)
    private String password;
    private String bio;
    private AppUserRole role;
    private boolean enabled = false;

    @Override
    public String toString() {
        return "AppUser{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", bio='" + bio + '\'' +
                ", role=" + role +
                ", enabled=" + enabled +
                '}';
    }
}
