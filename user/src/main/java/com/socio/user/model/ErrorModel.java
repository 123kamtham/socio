package com.socio.user.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class ErrorModel {
    private int code;
    private String message;
}
