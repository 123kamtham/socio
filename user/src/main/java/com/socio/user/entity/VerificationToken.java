package com.socio.user.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "verification_token")
public class VerificationToken {

    private static  final int EXPIRATION_TIME = 10;

    @Id
    @SequenceGenerator(
            name = "verification_token_sequence",
            sequenceName = "verification_token_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "verification_token_sequence"
    )
    private Long id;
    private String token;
    private Date expirationDate;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn( name = "appUser_id",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_APPUSER_VERIFY_TOKEN"))
    private AppUser appUser;

    public VerificationToken(String token, AppUser appUser) {
        super();
        this.token = token;
        this.expirationDate = calculateExpirationDate(EXPIRATION_TIME);
        this.appUser = appUser;
    }

    private Date calculateExpirationDate(int expirationTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(new Date().getTime());
        calendar.add(Calendar.MINUTE, expirationTime);
        return new Date(calendar.getTime().getTime());
    }

    @Override
    public String toString() {
        return "VerificationToken{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", expirationDate=" + expirationDate +
                ", appUser=" + appUser +
                '}';
    }
}
