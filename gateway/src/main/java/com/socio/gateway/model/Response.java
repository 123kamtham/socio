package com.socio.gateway.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;


@NoArgsConstructor
@Data
@AllArgsConstructor
@Builder
public class Response<T> {
    private boolean success;
    private T payload;
    private List<ErrorModel> errors;

    public Response(boolean success) {
        this(success,null, Collections.emptyList());
    }

    public Response(T payload){
        this(true,payload,Collections.emptyList());
    }

    public Response(Exception e, int errorcode){
        this(false,null, Collections.singletonList(new ErrorModel(errorcode, e.getMessage())));
    }
}
