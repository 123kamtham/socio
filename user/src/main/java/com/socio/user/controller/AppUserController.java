package com.socio.user.controller;

import com.socio.user.entity.AppUser;
import com.socio.user.model.*;
import com.socio.user.service.AppUserService;
import com.socio.user.unitily.ErrorCodes;
import com.socio.user.unitily.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/user")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;


    @PostMapping("/register")
    public Response<Boolean> registerUser(@RequestBody AppUserRegistrationInfo appUserRegistrationInfo, final HttpServletRequest request) {
        try {
            return appUserService.registerUser(appUserRegistrationInfo, request);
        } catch (Exception e) {
            return new Response<>(e, ErrorCodes.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/verifyRegistration")
    public Response<Boolean> verifyRegistration(@RequestParam("token") String token) {
//        try {
            return appUserService.verifyRegistration(token);
//        } catch (Exception e) {
//            return new Response<>(e, ErrorCodes.INTERNAL_SERVER_ERROR);
//        }
    }



    @GetMapping("/getAppUserByEmail")
    public Response<AppUser> getAppUserByEmail(@RequestParam("email") String email){
        try{
            return appUserService.getAppUserByEmail(email);
        }catch (Exception e){
            return new Response<>(e, ErrorCodes.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/requestResetPassword", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Response<Boolean> requestResetPassword(@RequestParam(value = "email") String email, final HttpServletRequest request) {
        try {
            return appUserService.requestResetPassword(email, request);
        } catch (Exception e) {
            return new Response<>(e, ErrorCodes.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/resetPassword", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<Boolean> resetPassword(@RequestParam("token") String token,@RequestBody PasswordModel
                                          passwordModel) {
        try {
            return appUserService.resetPassword(token, passwordModel.getNewPassword());
        } catch (Exception e) {
            return new Response<>(e, ErrorCodes.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/changePassword")
    public Response<Boolean> changePassword(@RequestBody PasswordModel passwordModel) {
        try {
            return appUserService.changePassword(passwordModel);
        } catch (Exception e) {
            return new Response<>(e, ErrorCodes.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/usernameAvailability")
    public Response<Boolean> isUsernameAvailable(@RequestParam("username") String username) {
        try {
            Boolean isUsernameAvailable = appUserService.isUsernameAvailable(username);
            return new Response<>(isUsernameAvailable);
        } catch (Exception e) {
            return new Response<>(e, ErrorCodes.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/getProfile")
    public Response<AppUserProfile> getAppUserProfile(@RequestParam("email") String email) {
        try {
            return appUserService.getAppUserProfile(email);
        } catch (Exception e) {
            return new Response<>(e, ErrorCodes.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/updateProfile")
    public Response<Boolean> updateAppUserProfile(@RequestBody AppUserProfile appUserProfile) {
        try {
            return appUserService.updateAppUserProfile(appUserProfile);
        } catch (Exception e) {
            return new Response<>(e, ErrorCodes.INTERNAL_SERVER_ERROR);
        }
    }
}
