package com.socio.user.unitily;

public class StringUtils {
    public static final String EMPTY = "";

    public static boolean isEmpty(String s) {

        return null == s || EMPTY.equals(s.trim());
    }

    public static boolean isNotEmpty(String s) {

        return !isEmpty(s);
    }

    public static String toLowerCase(String s) {

        return isEmpty(s) ? EMPTY : s.toLowerCase();
    }

    public static String defaultIfEmpty(String s) {

        return isEmpty(s) ? EMPTY : s;
    }

    public static String captialize(String s) {

        return isEmpty(s) ? EMPTY : (s.substring(0, 1).toUpperCase() + s
                .substring(1).toLowerCase());
    }
}
