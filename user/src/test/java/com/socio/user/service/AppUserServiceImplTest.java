package com.socio.user.service;

import com.socio.user.entity.AppUser;
import com.socio.user.entity.VerificationToken;
import com.socio.user.enums.AppUserRole;
import com.socio.user.event.AuthenticationViaEmail;
import com.socio.user.model.AppUserProfile;
import com.socio.user.model.AppUserRegistrationInfo;
import com.socio.user.model.ErrorModel;
import com.socio.user.model.Response;
import com.socio.user.repository.AppUserRepository;
import com.socio.user.repository.VerificationTokenRepository;
import com.socio.user.unitily.ErrorCodes;
import com.socio.user.unitily.StringConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;

public class AppUserServiceImplTest {

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private AppUserServiceImpl appUserService;

    @Mock
    private AppUserRepository appUserRepository;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Captor
    private ArgumentCaptor<AuthenticationViaEmail> captor;

    @Captor
    private ArgumentCaptor<AppUser> appUserArgumentCaptor;

    @Mock
    private VerificationTokenRepository verificationTokenRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @org.junit.Test
    public void registerUserIfUsernameIsAvailable() {
        AppUserRegistrationInfo appUserRegistrationInfo = AppUserRegistrationInfo.builder()
                .firstName("firstname")
                .lastName("lastname")
                .username("myusername")
                .email("myemail@gmail.com")
                .password("mypassword")
                .build();
        Mockito.when(appUserRepository.save(any())).thenReturn(AppUser.builder()
                .username("myusername")
                .email("myemail@gmail.com")
                .password("myencodedpassword")
                .role(AppUserRole.COMMON_USER)
                .build()
        );
        Mockito.when(appUserRepository.existsByUsername(any())).thenReturn(false);
        HttpServletRequest request = getRequest();
        Response<Boolean> response = appUserService.registerUser(appUserRegistrationInfo, request);
        Mockito.verify(applicationEventPublisher).publishEvent(captor.capture());
        AuthenticationViaEmail event = captor.getValue();
        Assert.assertEquals("http://myservername:8000/mycontextpath", event.getApplicationUrl());
        Assert.assertTrue(response.isSuccess());
        Assert.assertTrue(response.getPayload());
    }

    @Test
    public void donotRegisterUserIfUsernameAlreadyExist() {
        AppUserRegistrationInfo appUserRegistrationInfo = AppUserRegistrationInfo.builder()
                .username("myusername")
                .email("myemail@gmail.com")
                .password("mypassword")
                .build();
        Mockito.when(appUserRepository.existsByUsername(any())).thenReturn(true);
        Response<Boolean> response = appUserService.registerUser(appUserRegistrationInfo, null);
        Assert.assertTrue(response.isSuccess());
        Assert.assertFalse(response.getPayload());
    }

    @Test
    public void usernameAvailableIfDoestNotExistInData() {
        Mockito.when(appUserRepository.existsByUsername(any())).thenReturn(false);
        boolean response = appUserService.isUsernameAvailable("myusername");
        Assert.assertTrue(response);
    }

    @Test
    public void updateAppUserProfile(){
        AppUser appUser = AppUser.builder()
                        .username("venkatesh")
                        .password("1234")
                        .email("12@1.com")
                        .bio("bio").build();
        AppUserProfile appUserProfile = AppUserProfile.builder().username("venkatesh")
                .firstname("venkatesh")
                .lastname("kamtham")
                .email("12@1.com")
                .build();
        Mockito.when(appUserRepository.findByEmail(any())).thenReturn(appUser);
        Mockito.when(appUserRepository.save(any())).thenReturn(any());
        Response<Boolean> response = appUserService.updateAppUserProfile(appUserProfile);
        Mockito.verify(appUserRepository).save(appUserArgumentCaptor.capture());
        AppUser appUser1 = appUserArgumentCaptor.getValue();
        Assert.assertTrue(response.getPayload());
        Assert.assertEquals("venkatesh",appUser1.getFirstname());
    }

    @Test
    public void donotUpdateAppUserProfileIfAppuserNotFound(){
        AppUser appUser = AppUser.builder()
                .username("venkatesh")
                .password("1234")
                .email("12@1.com")
                .bio("bio").build();
        AppUserProfile appUserProfile = AppUserProfile.builder().username("venkatesh")
                .firstname("venkatesh")
                .lastname("kamtham")
                .email("12@1.com")
                .build();
        Mockito.when(appUserRepository.findByEmail(any())).thenReturn(null);
        Mockito.when(appUserRepository.save(any())).thenReturn(any());
        Response<Boolean> response= appUserService.updateAppUserProfile(appUserProfile);
        Assert.assertTrue(response.isSuccess());
        Assert.assertEquals(response.getErrors().get(0),new ErrorModel(ErrorCodes.NOT_FOUND, StringConstants.USER_NOT_EXIST));
    }


    @Test
    public void donotUpdateAppUserProfileIfChangedUsernameNotAvailable(){
        AppUser appUser = AppUser.builder()
                .username("venkatesh123")
                .password("1234")
                .email("12@1.com")
                .bio("bio").build();
        AppUserProfile appUserProfile = AppUserProfile.builder().username("venkatesh")
                .firstname("venkatesh")
                .lastname("kamtham")
                .email("12@1.com")
                .build();
        Mockito.when(appUserRepository.findByEmail(any())).thenReturn(appUser);
        Mockito.when(appUserRepository.save(any())).thenReturn(appUser);
        Mockito.when(appUserRepository.existsByUsername(any())).thenReturn(true);
        Response<Boolean> response= appUserService.updateAppUserProfile(appUserProfile);
        Assert.assertTrue(response.isSuccess());
        Assert.assertEquals(response.getErrors().get(0),new ErrorModel(ErrorCodes.BAD_REQUEST, StringConstants.USERNAME_NOT_AVAILABLE));
    }

    @Test
    public void getAppUserProfile(){
        AppUser appUser = AppUser.builder()
                .username("venkatesh123")
                .password("1234")
                .email("12@1.com")
                .bio("bio").build();
        AppUserProfile appUserProfile = AppUserProfile.builder().username("venkatesh123")
                .email("12@1.com")
                .bio("bio")
                .build();
        Mockito.when(appUserRepository.findByEmail(any())).thenReturn(appUser);
        Response<AppUserProfile> response= appUserService.getAppUserProfile("12@1.com");
        Assert.assertEquals(appUserProfile,response.getPayload());
    }

    @Test
    public void giveErrorResponseInGetAppUserProfileUserNotPresent(){
        Mockito.when(appUserRepository.findByEmail(any())).thenReturn(null);
        Response<AppUserProfile> response= appUserService.getAppUserProfile("12@1.com");
        Assert.assertEquals(response.getErrors().get(0),new ErrorModel(ErrorCodes.NOT_FOUND, StringConstants.USER_NOT_EXIST));
    }

    @Test
    public void verifyRegistration() {
        String token = "mytoken";
        AppUser appUser = AppUser.builder()
                .username("myuser")
                .email("email@gmail.com")
                .password("pass")
                .role(AppUserRole.COMMON_USER)
                .build();
        VerificationToken verificationToken = new VerificationToken(token,appUser);
        Mockito.when(verificationTokenRepository.findByToken(any())).thenReturn(verificationToken);
        Response<Boolean> response = appUserService.verifyRegistration(token);
        System.out.println(response);
        Assert.assertTrue(response.isSuccess());
        Assert.assertTrue(response.getPayload());
    }


    private HttpServletRequest getRequest() {
        return new HttpServletRequest() {
            @Override
            public String getAuthType() {
                return null;
            }

            @Override
            public Cookie[] getCookies() {
                return new Cookie[0];
            }

            @Override
            public long getDateHeader(String s) {
                return 0;
            }

            @Override
            public String getHeader(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getHeaders(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getHeaderNames() {
                return null;
            }

            @Override
            public int getIntHeader(String s) {
                return 0;
            }

            @Override
            public String getMethod() {
                return null;
            }

            @Override
            public String getPathInfo() {
                return null;
            }

            @Override
            public String getPathTranslated() {
                return null;
            }

            @Override
            public String getContextPath() {
                return "/mycontextpath";
            }

            @Override
            public String getQueryString() {
                return null;
            }

            @Override
            public String getRemoteUser() {
                return null;
            }

            @Override
            public boolean isUserInRole(String s) {
                return false;
            }

            @Override
            public Principal getUserPrincipal() {
                return null;
            }

            @Override
            public String getRequestedSessionId() {
                return null;
            }

            @Override
            public String getRequestURI() {
                return null;
            }

            @Override
            public StringBuffer getRequestURL() {
                return null;
            }

            @Override
            public String getServletPath() {
                return null;
            }

            @Override
            public HttpSession getSession(boolean b) {
                return null;
            }

            @Override
            public HttpSession getSession() {
                return null;
            }

            @Override
            public String changeSessionId() {
                return null;
            }

            @Override
            public boolean isRequestedSessionIdValid() {
                return false;
            }

            @Override
            public boolean isRequestedSessionIdFromCookie() {
                return false;
            }

            @Override
            public boolean isRequestedSessionIdFromURL() {
                return false;
            }

            @Override
            public boolean isRequestedSessionIdFromUrl() {
                return false;
            }

            @Override
            public boolean authenticate(HttpServletResponse httpServletResponse) throws IOException, ServletException {
                return false;
            }

            @Override
            public void login(String s, String s1) throws ServletException {

            }

            @Override
            public void logout() throws ServletException {

            }

            @Override
            public Collection<Part> getParts() throws IOException, ServletException {
                return null;
            }

            @Override
            public Part getPart(String s) throws IOException, ServletException {
                return null;
            }

            @Override
            public <T extends HttpUpgradeHandler> T upgrade(Class<T> aClass) throws IOException, ServletException {
                return null;
            }

            @Override
            public Object getAttribute(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getAttributeNames() {
                return null;
            }

            @Override
            public String getCharacterEncoding() {
                return null;
            }

            @Override
            public void setCharacterEncoding(String s) throws UnsupportedEncodingException {

            }

            @Override
            public int getContentLength() {
                return 0;
            }

            @Override
            public long getContentLengthLong() {
                return 0;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public ServletInputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public String getParameter(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getParameterNames() {
                return null;
            }

            @Override
            public String[] getParameterValues(String s) {
                return new String[0];
            }

            @Override
            public Map<String, String[]> getParameterMap() {
                return null;
            }

            @Override
            public String getProtocol() {
                return null;
            }

            @Override
            public String getScheme() {
                return null;
            }

            @Override
            public String getServerName() {
                return "myservername";
            }

            @Override
            public int getServerPort() {
                return 8000;
            }

            @Override
            public BufferedReader getReader() throws IOException {
                return null;
            }

            @Override
            public String getRemoteAddr() {
                return null;
            }

            @Override
            public String getRemoteHost() {
                return null;
            }

            @Override
            public void setAttribute(String s, Object o) {

            }

            @Override
            public void removeAttribute(String s) {

            }

            @Override
            public Locale getLocale() {
                return null;
            }

            @Override
            public Enumeration<Locale> getLocales() {
                return null;
            }

            @Override
            public boolean isSecure() {
                return false;
            }

            @Override
            public RequestDispatcher getRequestDispatcher(String s) {
                return null;
            }

            @Override
            public String getRealPath(String s) {
                return null;
            }

            @Override
            public int getRemotePort() {
                return 0;
            }

            @Override
            public String getLocalName() {
                return null;
            }

            @Override
            public String getLocalAddr() {
                return null;
            }

            @Override
            public int getLocalPort() {
                return 0;
            }

            @Override
            public ServletContext getServletContext() {
                return null;
            }

            @Override
            public AsyncContext startAsync() throws IllegalStateException {
                return null;
            }

            @Override
            public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException {
                return null;
            }

            @Override
            public boolean isAsyncStarted() {
                return false;
            }

            @Override
            public boolean isAsyncSupported() {
                return false;
            }

            @Override
            public AsyncContext getAsyncContext() {
                return null;
            }

            @Override
            public DispatcherType getDispatcherType() {
                return null;
            }
        };
    }

}