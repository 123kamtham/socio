package com.socio.user.service;

import com.socio.user.model.MailModel;
import com.socio.user.unitily.RedirectRequestPath;
import com.socio.user.unitily.StringConstants;
import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

//import javax.mail.MessagingException;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Slf4j
@Service
public class MailServiceImpl implements MailService{

//    @Autowired
//    private  JavaMailSender mailSender;

    @Override
    public void sendRedirectRequestPathViaMail(String redirectToRequestPath, String email, String verificationUrl) {
        switch (redirectToRequestPath){
            case RedirectRequestPath.VERIFY_REGISTRATION:
                sendRegistrationVerificationMail(email,verificationUrl);
                break;
            case RedirectRequestPath.RESET_PASSWORD:
                sendResetPasswordMail(email,verificationUrl);
                break;
            default:
                break;
        }
    }

    public void sendEmail(MailModel mail) {
//        MimeMessage mimeMessage = mailSender.createMimeMessage();
//
//        try {
//
//            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
//
//            mimeMessageHelper.setSubject(mail.getMailSubject());
//            mimeMessageHelper.setFrom(new InternetAddress(mail.getMailFrom(), StringConstants.APP_NAME));
//            mimeMessageHelper.setTo(mail.getMailTo());
//            mimeMessageHelper.setText(mail.getMailContent());
//
////            mailSender.send(mimeMessageHelper.getMimeMessage());
//
//        } catch (MessagingException | UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
    }

    private void sendRegistrationVerificationMail(String email, String verificationUrl) {
        MailModel mailModel = MailModel.builder()
        .mailFrom(StringConstants.MAIL_FROM)
        .mailTo(email)
        .mailContent(StringConstants.REGISTRATION_VERIFICATION_MAIL_CONTENT + verificationUrl)
        .mailSubject(StringConstants.REGISTRATION_VERIFICATION_MAIL_SUBJECT)
        .build();
        sendEmail(mailModel);
        log.info("Click the link to verify your account: {}",verificationUrl);
    }

    private void sendResetPasswordMail(String email, String verificationUrl) {
        MailModel mailModel = MailModel.builder()
                .mailFrom(StringConstants.MAIL_FROM)
                .mailTo(email)
                .mailContent(StringConstants.RESET_PASSWORD_MAIL_CONTENT + verificationUrl)
                .mailSubject(StringConstants.RESET_PASSWORD_MAIL_SUBJECT)
                .build();
        sendEmail(mailModel);
        log.info("Click the link to reset password: {}",verificationUrl);
    }
}
